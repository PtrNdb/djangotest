"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from strony.views import home_view,kontakt,sklep,social_media,Onas
from produkty.views import produkt_opis_widok, produkt_utworz_widok


urlpatterns = [

    path('', home_view, name='home'),
    path('kontakt/', kontakt),
    path('sklep/', sklep),
    path('social_media/', social_media),
    path('produkty/', produkt_opis_widok),
    path('formularz/', produkt_utworz_widok),
    path('Onas/', Onas),
    path('admin/', admin.site.urls),
    path('users/', include('users.urls'))
]


