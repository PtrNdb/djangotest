from django import forms
from .models import Produkt

class ProduktForm(forms.ModelForm):
    nazwa = forms.CharField(label='',
        widget=forms.TextInput(attrs={"placeholder": "Wpisz cos"}))
    email = forms.EmailField()
    opis = forms.CharField(
                    required=False,
                    widget= forms.Textarea(
                        attrs={
                            "placeholder":"wpisz coś",
                            "class":"nowa-klasa",
                            "id":"moje-id-dla-polatekstowego",
                            "rows":20,
                            "cols":100
                        }
                    )
    )
    cena = forms.DecimalField(initial=269.99)
    class Meta:
        model = Produkt
        fields = [
            'nazwa',
            'opis',
            'cena'
        ]
    def clean_nazwa(self,*args,**kwargs):
        nazwa = self.cleaned_data.get("nazwa")
        if not "Piotr" in nazwa:
            raise forms.ValidationError("TO jest niepoprawna nazwa")
        if not "Pawel" in nazwa:
            raise forms.ValidationError("TO jest niepoprawna nazwa")
        return nazwa
    def clean_email(self,*args,**kwargs):
        email = self.cleaned_data.get("email")
        if not email.endswith("pl"):
            raise forms.ValidationError("niepoprawny e-mail")
        return email

class NowyProduktForm(forms.Form):
    nazwa = forms.CharField(label= '', widget=forms.TextInput(attrs={"placeholder":"Wpisz cos"}))
    opis = forms.CharField(
                    required=False,
                    widget= forms.Textarea(
                        attrs={
                            "placeholder":"wpisz coś",
                            "class":"nowa-klasa",
                            "id":"moje-id-dla-polatekstowego",
                            "rows":20,
                            "cols":100
                        }
                    )
    )
    cena = forms.DecimalField(initial=269.99)
