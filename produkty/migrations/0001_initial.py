# Generated by Django 3.0.2 on 2020-02-15 15:41

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Produkt',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nazwa', models.TextField()),
                ('opis', models.TextField()),
                ('cena', models.TextField()),
            ],
        ),
    ]
