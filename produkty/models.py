from django.db import models

# Create your models here.
class Produkt(models.Model):
    nazwa = models.CharField(max_length=250)
    opis = models.TextField(blank=True, null=True)
    cena = models.DecimalField(max_digits=1000, decimal_places=2)
    podsumowanie = models.TextField(default = "Django")
    przecena = models.BooleanField(default=False)