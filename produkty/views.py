from django.shortcuts import render
from .forms import ProduktForm, NowyProduktForm
from .models import Produkt

# def produkt_utworz_widok(request):
#     nowy_form = NowyProduktForm
#     if request.method == "POST":
#         nowy_form = NowyProduktForm(request.POST)
#         if nowy_form.is_valid():
#             print(nowy_form.cleaned_data)
#             Produkt.object.create(nazwa=nowy_opis)
#         else:
#             print(nowy_form.errors)
#     context = {
#         "form" : nowy_form
#     }
#     return render(request, "produkty/produkt_utworz.html", context)

def produkt_utworz_widok(request):
    form = ProduktForm(request.POST or None)
    if form.is_valid():
        form.save(commit = True)
        form = ProduktForm()
    context = {
        "form" :form
    }
    return render(request, "produkty/produkt_utworz.html", context)

def produkt_opis_widok(request):
    obj = Produkt.objects.get(id=1)
    kontekst = {
        'obiekt' : obj
    }
    return render(request, "produkty/opis.html", kontekst)