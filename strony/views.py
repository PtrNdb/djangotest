from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
def home_view(request,*args,**kwargs):
    print(args,kwargs)
    print(request.user)
    return render(request, "dom.html", {})

def kontakt(request,*args,**kwargs):
    return render(request, "Kontakt.html", {})

def Onas(request,*args,**kwargs):
    kontekst = {
        "opis_strony": "Moja strona internetowa",
        "numer_telefonu": 123456,
        "moja_lista": [123,456,789,123,'Kononowicz'],
        "zmienna_html": "<h1> Dzisiaj mocno pyli brzoza </h1>"
    }
    return render(request, "Onas.html", kontekst)

def social_media(request,*args,**kwargs):
    return HttpResponse("<h1>Facebook, LinkedIn</h1>")

def sklep(request,*args,**kwargs):
    return HttpResponse("<h1>Produkty : korki, buty, piłka</h1>")