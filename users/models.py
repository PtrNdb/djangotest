from django.db import models

# Create your models here.

class User(models.Model):
    #klucz głowny
    id = models.IntegerField(primary_key=True)
    # ciąg znakowy odwzorowanie varchara
    email = models.CharField(max_length=255)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    age = models.IntegerField()
    active = models.BooleanField(default=False)
    password = models.CharField(max_length=255, null=True)

    def __str__(self):
        return f"id: {self.id}," \
               f" email: {self.email}," \
               f" first_name: {self.first_name}" \
               f" last_name: {self.last_name}" \
               f" age: {self.age}" \
               f" active: {self.active}" \
               f" password: {self.password}"


