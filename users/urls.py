from django.urls import path
from users import views

urlpatterns = [
    path('', views.get_users),
    # localhost:8000/users/create
    path('create', views.create_user),
    # localhost:8000/users/get/123
    path('get/<int:id>', views.get_user_by_id),

    path('delete/<int:id>', views.delete_user_by_id),

    path('update/<int:id>',views.update_user),
    path('activate/<int:id>',views.activate_user),
    path('deactivate/<int:id>',views.deactivate_user),
    path('changepassword/<int:id>',views.changepassword),

]