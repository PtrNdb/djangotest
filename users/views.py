import json

from django.core import serializers
from django.http import HttpResponse

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from users.models import User


@csrf_exempt
@require_http_methods('GET')
def get_users(request):
    users = User.objects.all()
    print(list(users))
    data = serializers.serialize('json', users)

    return HttpResponse(data, content_type="application/json")


@csrf_exempt
@require_http_methods("POST")
def create_user(request):
    body = request.body.decode("UTF-8")
    jsonbody = json.loads(body)

    try:
        email = jsonbody["email"]
    except:
        return HttpResponse("Email not provided", status=409)

    first_name = jsonbody["firstName"]
    last_name = jsonbody["lastName"]
    age = jsonbody["age"]
    password = jsonbody["password"]

    user = User(email=email,
                first_name=first_name,
                last_name=last_name,
                age=age,
                password=password)

    user.active = True
    user.save()
    print(user)

    return HttpResponse("")


@require_http_methods("GET")
def get_user_by_id(request,id):
    print(id)
    users = User.objects.filter(id=int(id))
    print(users)
    return HttpResponse("")

@csrf_exempt
@require_http_methods("DELETE")
def delete_user_by_id(request,id):

    user_to_delete = User.objects.filter(id = int(id))

    if len(list(user_to_delete)) == 0:
        return HttpResponse("Nie ma takiego uzytkownika", status=404)
    user_to_delete.delete()
    return HttpResponse("Usunięto uzytkownika", status=204)

@csrf_exempt
@require_http_methods("PATCH")
def update_user(request,id):

    user_body = json.loads(request.body.decode("UTF_8"))
    first_name = user_body["firstName"]
    last_name = user_body["lastName"]
    age = user_body["age"]

    users = User.objects.filter(id=id)

    if(len(list(users))) == 0:
        return HttpResponse("Nie znaleziono uzytkownika", status=404)

    user = users[0]
    user.first_name = first_name
    user.last_name = last_name
    user.age = age
    user.save()

    return HttpResponse("Zaaktualizowano uzytkownia", status=200)

@csrf_exempt
@require_http_methods("PATCH")
def activate_user(request,id):

    users = User.objects.filter(id=id)


    if (len(list(users))) == 0:
        return HttpResponse("Nie znaleziono uzytkownika", status=404)

    user = users[0]
    user.active = True
    user.save()

    return HttpResponse("Aktywowano uzytkownia", status=200)

@csrf_exempt
@require_http_methods("PATCH")
def deactivate_user(request,id):

    users = User.objects.filter(id=id)

    if (len(list(users))) == 0:
        return HttpResponse("Nie znaleziono uzytkownika", status=404)

    user = users[0]
    user.active = False
    user.save()
    return HttpResponse("Zablokowano uzytkownia", status=200)

@csrf_exempt
@require_http_methods("PATCH")
def changepassword(request,id):


    #password = user_body["password"]
    users = User.objects.filter(id=id)

    if (len(list(users))) == 0:
        return HttpResponse("Nie znaleziono uzytkownika", status=404)

    user = users[0]
    if not user.active:
        return HttpResponse("Nie można zmienić hasła, użytkownik nie jest aktywny")
    user_body = json.loads(request.body.decode("UTF_8"))
    old_password = user_body("oldPassword")
    new_passowrd = user.body("newPassword")

    if not user.password == old_password:
       return HttpResponse("Podane stare hasło nie zgadza się",status=409)

    if old_password == new_password:
        return HttpResponse("Nowe hasło nie jest różne", status=409)

    user.password = new_passowrd
    user.save()
    return HttpResponse("Hasło zostało zaaktualizowane", status=200)



    user.password = password
    user.save()

    return HttpResponse("zmieniono hasło", status=200)


